#!/usr/bin/python3
# -*- coding: utf-8 -*-
import logging
import re

import jack
from pythonosc import dispatcher
from pythonosc import osc_server

# KEYBOARD_REGEXP = ".*MK-361 USB MIDI"
KEYBOARD_REGEXP = ".*USB Uno MIDI Interface"
PROGRAMS = ["Pianoteq", "setBfree*"]

log = logging.getLogger(__name__)


def print_volume_handler(addr, args):
    print(args)


def main():
    global jack_client
    logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")

    jack_client = jack.Client('keyboard-connect')
    try:
        # connect to the first instrument
        first_instrument_program_name = PROGRAMS[0]
        log.info("Connecting keyboard to the first instrument: {!r}".format(first_instrument_program_name))
        activate_instrument(first_instrument_program_name, jack_client)
    except Exception as e:
        log.exception(e)
    try:
        # Start OSC server
        my_dispatcher = dispatcher.Dispatcher()
        my_dispatcher.map("/instrument_change", load_instrument_callback)
        server = osc_server.ThreadingOSCUDPServer(("127.0.0.1", 5005), my_dispatcher)
        print("Listening on {}".format(server.server_address))
        server.serve_forever()
    finally:
        jack_client.close()


def load_instrument_callback(_entry_point, *args):
    """OSC callback to change instrument
    """
    global jack_client

    log.info("* Received OSC: /instrument_change {!r}".format(args))
    instrument_id = args[0]
    if not isinstance(instrument_id, int):
        log.error("Unexpected argument type. Excepted: {!r}, got {!r}".format(int, instrument_id))
        return
    if instrument_id > len(PROGRAMS):
        raise Exception("Invalid instrument id: {!r}. Max: {}".format(instrument_id, len(PROGRAMS)))
    instrument_name = PROGRAMS[instrument_id - 1]
    log.info("Activating {}".format(instrument_name))
    instrument_regex = ".*{}".format(instrument_name)
    activate_instrument(instrument_regex, jack_client)


def find_jack_port(client, type, port_regexp, raise_if_not_found=True):
    """type must be "input" or "output"
    """
    assert port_regexp
    args = {
        "input": {"is_input": True},
        "output": {"is_output": True},
    }[type]
    matching_ports = list(filter(lambda p: re.match(port_regexp, p.name),
                                 client.get_ports(**{"is_midi": True, **args})))
    if raise_if_not_found and len(matching_ports) == 0:
        raise Exception("Didn't found any port using regex {!r}".format(port_regexp))
    assert len(matching_ports) == 1, "Found more than one port using regex {!r}".format(port_regexp)
    return matching_ports[0] if matching_ports else None


def get_connection_between(client, port, program_name):
    """Check whenever a connection exists between given port and regex and return port of program corresponding to
    this connection
        - return None if no connection found
    """
    connections = client.get_all_connections(port)
    return find_first(lambda p: re.match(program_name, p.name), connections)


def activate_instrument(instrument_program_name, client):
    """Connect keyboard to given program (found by rexep)
    """
    # Find keyboard port
    keyboard_port = find_jack_port(jack_client, 'output', KEYBOARD_REGEXP)
    log.info("Found keyboard port: {}".format(keyboard_port))
    # Find input program port
    program_port = find_jack_port(client, 'input', instrument_program_name)
    # Disconnect keyboard from other programs
    for program_name in PROGRAMS:
        if not program_name == instrument_program_name:
            port_connections = client.get_all_connections(keyboard_port)
            if port_connections:
                # Is keyboard connected to this program ?
                connection = get_connection_between(client, keyboard_port, program_name)
                if connection:
                    log.info("Disconnecting {!r} from {!r}".format(keyboard_port.name, program_port.name))
                    client.disconnect(keyboard_port, connection)
    # Check if connection doesn't already exists
    if get_connection_between(client, keyboard_port, instrument_program_name):
        log.info("   -> Already existing connection")
        return
    # Connect
    log.info("Connecting {!r} -> {!r}".format(keyboard_port.name, program_port.name))
    client.connect(keyboard_port, program_port)


def find_first(_lambda, where):
    """Return first occurence of object that match given _lambda function in where, or None if not found
    """
    return next((obj for obj in where if _lambda(obj)), None)


if __name__ == "__main__":
    main()
