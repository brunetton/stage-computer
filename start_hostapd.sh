#!/bin/bash
set -x

rfkill unblock wlan
sleep 2
nmcli radio wifi off
sleep 2
ifconfig wlan1 10.15.0.1/24 up
sleep 2
pkill -fe dhcpd
killall hostapd -9
sleep 2
hostapd ~bruno2/stage/hostapd.conf &
sleep 2
dhcpd -d -f  -cf ~bruno2/stage/dhcpd.conf wlan1 &
