#!/bin/bash
set -x

# Restore volumes
sudo alsactl restore

pkill -ef pnmixer  # it auto starts pulseaudio !

# pkill -ef qjackctl
# pkill -ef jackd
pkill -ef jack-matchmaker
pkill -ef Pianoteq
pkill -ef open-stage-control
pkill -ef setBfree
pkill -ef switch.py

pulseaudio -k
sleep 0.5
pkill -ef pulseaudio

qjackctl -s &  # also starts a2jmidid
sleep 1
jack-matchmaker -p ~/stage/jackmatchmaker.conf &
"/home/bruno2/bin/Pianoteq 6 STAGE/amd64/Pianoteq 6 STAGE" &

open-stage-control -n -m openstage:virtual -l ~/stage/osc.json -s localhost:5005 &
setBfree -c ~/stage/setBfree.cfg -p ~/stage/setBfree.pgm &

# Start switch server
sleep 2; ~/stage/server/switch.py &
